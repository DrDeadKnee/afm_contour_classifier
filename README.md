# AFM IMAGE ANALYSIS
This software is designed to help in counting and quantifying small objects above a slightly rough
surface. For manually exploring the data, functions contained in 'img_functions.py' are useful, as
shown in api_examples.ipynb.
Manual classification of contours using the manual_labelling.py file is now available. 
Check out api_examples.ipynb to get an idea of where to start.

## INSTALLATION
$ pip install -r requirements.txt

## RUNNING TESTS
$ nosetests tests

## MANUAL CLASSIFICATION
$ python main.py <image_folder_path> <database_path>

### TODO
- Fix augmented table call
- Write automatic categorization module
- Check .tif r/w
