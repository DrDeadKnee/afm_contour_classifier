from classify.ui import ask, choose
import os
import random
import img_functions


class LabelData(object):

    def __init__(self, dbmanager, img_path):
        self.DBM = dbmanager
        self.img_path = img_path
        self.lab_frame = self.DBM.LabMan.load_new_df()
        self.z_threshold = None
        self.z_binwidth = None
        self.classopts = [i[0] for i in self.DBM.LabMan.read_unique('label')]

    def choose_z_params(self):
        options = [
            "Fixed Thresholds",
            "Varied Thrsholds"
        ]

        question = "Do you want a single, fixed detection threshold or many random ones?"
        choice = choose(question, options)
        z_opt = choice.split()[0]

        if z_opt == 'Fixed':
            z = ask('What is your z threshold? (nm)', float)
            self.z_threshold = z

        else:
            self.low_zlim = ask('What is the min value for your z threshold (nm)?', float)
            self.high_zlim = ask('What is the max value for your z threshold (nm)?', float)
            self.z_threshold = 'random'

        question = 'What do you want binwidth for zeroing to be (default = 0.098 nm)?'
        self.z_binwidth = ask(question, float)

    def label(self):
        if isinstance(self.z_threshold, float):
            while True:
                self.label_single(self.z_threshold, self.z_binwidth)
        elif self.z_threshold == 'random':
            while True:
                z = random.uniform(self.low_zlim, self.high_zlim)
                self.label_single(z, self.z_binwidth)

    def label_single(self, z_threshold, z_binwidth):
        files = os.listdir(self.img_path)
        filename = os.path.join(self.img_path, random.choice(files))
        height, phase = img_functions.load_scan(filename)
        height = img_functions.shift_img(height, z_binwidth)

        contours = img_functions.get_contours(height, z_threshold)
        if len(contours) > 0:
            selected_contour = [random.choice(contours)]

            new_entry = self.DBM.LabMan.populate_from_contour(
                selected_contour, height, z_threshold=z_threshold, z_binsize=z_binwidth
            )
            new_entry.img_name = filename.split("/")[1]

            img_functions.plot_contours(height, contours=selected_contour, interactive=True)
            options = ["yes", "no", "unsure", "save & quit"]
            choice = choose("Is this a reasonable outline for a single object?", options)

            if choice == 'yes':
                new_entry.is_contour = 1
                new_entry.label = self.choose_class()
                self.lab_frame = self.lab_frame.append(new_entry.to_dict(), ignore_index=True)
            elif choice == 'no':
                new_entry.is_contour = 0
                new_entry.label = 'false_pos'
                self.lab_frame = self.lab_frame.append(new_entry.to_dict(), ignore_index=True)
            elif choice == 'unsure':
                pass
            elif choice == "save & quit":
                self.DBM.LabMan.save_data(self.lab_frame)
                exit()

            img_functions.clear_plots()

    def choose_class(self):
        question = 'Which label best describes the object in contours?'
        options = self.classopts + ['None - add new label']
        choice = choose(question, options)
        if choice == options[-1]:
            choice = ask('What would you like to call the new label?', str)
            self.classopts.append(choice)
        return choice
