import numpy as np
import pandas as pd


class BadArgumentError(Exception):
    pass


def evaluate_predictions(predictions, labels, negative_labels=[0]):
    confusion_masks = confusion_matrix_masks(predictions, labels, negative_labels)
    accuracy = get_accuracy(predictions, labels)
    f1 = get_f1(confusion_masks=confusion_masks)
    recall = get_recall(confusion_masks=confusion_masks)
    precision = get_precision(confusion_masks=confusion_masks)
    return {
        "confusion_masks": confusion_masks,
        "accuracy": accuracy,
        "f1": f1,
        "recall": recall,
        "precision": precision
    }


def get_accuracy(predictions, labels):
    predictions, labels = np.asarray(predictions), np.asarray(labels)
    correct_predictions = predictions[predictions == labels]
    return float(len(correct_predictions)) / len(predictions)


def get_f1(predictions=None, labels=None, confusion_masks=None, negative_labels=[0]):
    if confusion_masks is None:
        if predictions is None or labels is None:
            raise BadArgumentError(
                "Must supply confusion masks if not supplying predictions and labels."
            )
        confusion_masks = confusion_matrix_masks(predictions, labels, negative_labels)
    recall = get_recall(confusion_masks=confusion_masks)
    precision = get_precision(confusion_masks=confusion_masks)

    return 2 * (precision * recall) / (precision + recall)


def get_recall(predictions=None, labels=None, confusion_masks=None, negative_labels=[0]):
    if confusion_masks is None:
        if predictions is None or labels is None:
            raise BadArgumentError(
                "Must supply confusion masks if not supplying predictions and labels."
            )
        confusion_masks = confusion_matrix_masks(predictions, labels, negative_labels)

    tp = np.sum(confusion_masks['tp'])
    fn = np.sum(confusion_masks['np'])

    return float(tp) / (tp + fn)


def get_precision(predictions=None, labels=None, confusion_masks=None, negative_labels=[0]):
    if confusion_masks is None:
        if predictions is None or labels is None:
            raise BadArgumentError(
                "Must supply confusion masks if not supplying predictions and labels."
            )
        confusion_masks = confusion_matrix_masks(predictions, labels, negative_labels)

    tp = np.sum(confusion_masks['tp'])
    fp = np.sum(confusion_masks['fp'])

    return float(tp) / (tp + fp)


def confusion_matrix_masks(predictions, labels, negative_labels=[0]):
    if not isinstance(negative_labels, list):
        negative_labels = [negative_labels]

    predictions, labels = np.asarray(predictions), np.asarray(labels)
    all_negative_masks = [predictions == i for i in negative_labels]
    positive_mask = ~all_negative_masks[0]
    negative_mask = all_negative_masks[0]
    for i in range(1, len(all_negative_masks)):
        positive_mask = positive_mask & ~all_negative_masks[i]
        negative_mask = negative_mask & all_negative_masks[i]

    correct_mask = predictions == labels

    return {
        "tp": correct_mask & positive_mask,
        "fp": ~correct_mask & positive_mask,
        "tn": correct_mask & negative_mask,
        "fn": ~correct_mask & negative_mask
    }


def number_unique_strings(list_of_strings):
    list_of_ints = np.asarray(list_of_strings)
    unique = np.unique(list_of_ints)
    for idx, key in enumerate(unique):
        mask = list_of_ints == key
        list_of_ints[mask] = idx

    return list_of_ints


def whiten_matrix(matrix):
    cov = np.cov(matrix.T)
    Ul, D, Ur = np.linalg.svd(cov)
    W = D**-0.5 * Ul.T
    whitened = np.dot(W, matrix.T).T

    if isinstance(matrix, pd.DataFrame):
        whitened = pd.DataFrame(whitened, columns=matrix.keys())

    return whitened


def PCA(matrix, dim=2):
    cov = np.cov(matrix.T)
    eigenvalues, eigenvectors = np.linalg.eigh(cov)
    eigt = eigenvectors.T

    if isinstance(matrix, pd.DataFrame):
        keys, matrix = matrix.keys(), matrix.as_matrix()

    components = []
    for i in range(1, dim + 1):
        contributions = []
        for j in range(len(matrix)):
            contributions.append(np.dot(matrix[j], eigt[-i]))
        components.append(contributions)

    return components, eigt[-dim:], eigenvalues[-dim:]
