# options is a list of strings
def choose(query, options, cancelopt=False):
    print("\n(q to quit)")

    legit_choices = []
    querydisplay = query

    # TODO build implementation of 'Cancel' option into ui.choose
    if cancelopt and 'Cancel' not in options:
        options.append('Cancel')

    for number, option in enumerate(options):
        optiondisplay = "\n" + str(number + 1) + " - " + str(option)
        querydisplay += optiondisplay
        legit_choices.append(str(number + 1))
    querydisplay += "\n>"

    choice = raw_input(querydisplay)

    if choice == "q":
        exit()

    if choice in legit_choices:
        legitchoice = True
    else:
        legitchoice = False

    if legitchoice:
        return options[int(choice) - 1]

    else:
        print("That was not an option. Please enter one of the following:")
        for i in legit_choices:
            print(i)

        choice = choose(query, options)
        return choice


# flexible ask module which will take any specific type
def ask(query, variabletype=str, max_tries=5, current_tries=0):
    if current_tries >= max_tries:
        print("Maximum tries reached. Exiting")
        exit()

    else:
        print("\n(q to quit)")
        querydisplay = query + "\n>"
        user_answer = raw_input(querydisplay)

        if user_answer == "q":
            exit()

        processed_answer = _cast_type(user_answer, variabletype)
        if processed_answer is not None:
            final_answer = processed_answer
        else:
            final_answer = ask(
                query,
                variabletype=variabletype,
                max_tries=max_tries,
                current_tries=current_tries + 1
            )

    return final_answer


def _cast_type(var, vartype):
    try:
        response = vartype(var)
    except ValueError:
        print("Expected variable {}. Please try again.".format(vartype))
        response = None
    return response
