# Data Base Manager

import sqlite3
import os
from models.imginfo import ImgInfoManager
from models.processingsummary import ProcessingSummaryManager
from models.contours import ContourManager
from models.labelledcontour import LabelledContourManager
import pandas


class DBMan(object):

    def __init__(self, path_to_db, load_labelling=False):
        self.cnx = self.initialize_db_link(path_to_db)
        self.ImgMan = ImgInfoManager(self.cnx)
        self.ProcMan = ProcessingSummaryManager(self.cnx)
        self.ContMan = ContourManager(self.cnx)
        if load_labelling:
            self.LabMan = LabelledContourManager(self.cnx)

    def initialize_db_link(self, path_to_db):
        if os.path.isfile(path_to_db):
            cnx = sqlite3.connect(path_to_db)
        else:
            print("Database not found at specified path. Launching empty database...")
            cnx = self.launch_db(path_to_db)

        return cnx

    def launch_db(self, path_to_db):
        cnx = sqlite3.connect(path_to_db)
        cur = cnx.cursor()
        schema = open('schema.sql')
        cur.executescript(schema.read())
        return cnx

    def get_connection(self):
        return self.cnx

    # This function looks wierd. It allows us to continually change tables without
    # re-writing though. It relies on inner join returning img_id before processingsummary
    def load_augmented_table(self):
        sql = """
            SELECT *
            FROM ImgInfo
            INNER JOIN ProcessingSummary ON ImgInfo.rowid = ProcessingSummary.img_id
        """
        table = pandas.read_sql(sql, con=self.cnx)
        newcolumns = []
        triggered = False
        for column in table:
            if column == 'rowid' and not triggered:
                newcolumns.append('IMG_ID_1')
                triggered = True
            elif column == 'rowid' and triggered:
                newcolumns.append('Proc_ID')
            else:
                newcolumns.append(column)
        table.columns = newcolumns
        return table

    def load_single_table(self, tablename):
        sql = "SELECT * FROM {}".format(tablename)
        return pandas.read_sql(sql, con=self.cnx)
