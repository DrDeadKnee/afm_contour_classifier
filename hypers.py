z_binsize = 0.098  # nm
z_threshold = 3.0  # nm


def to_dict():
    return {
        'z_binsize': z_binsize,
        'z_threshold': z_threshold
    }
