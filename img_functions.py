from matplotlib import pyplot as plt
import numpy as np
import seaborn as sns
import hypers
from pandas import DataFrame
import warnings
from skimage import measure
from PIL import Image
from mpl_toolkits import axes_grid1
from mpl_toolkits.axes_grid1 import anchored_artists

warnings.simplefilter('ignore', np.RankWarning)


def hist_img(img, title=None, xlabel=None, binwidth=None):
    if binwidth is None:
        binwidth = hypers.z_binsize

    img_as_array = np.asarray(np.asmatrix(img).flatten())[0]
    mybins = np.arange(np.amin(img_as_array), np.amax(img_as_array) + binwidth, binwidth)

    sns.set_style('ticks')
    sns.set_context('paper', font_scale=2.0)
    plt.figure(figsize=(8, 6))
    plt.hist(img_as_array.tolist(), bins=mybins)
    plt.show()


def get_img_stats(img):
    mat = np.asmatrix(img)
    return np.mean(mat), np.std(mat)


def get_img_centre(img, binwidth=None):
    img_as_array = np.asarray(np.asmatrix(img).flatten())[0]

    if binwidth is None:
        binwidth = hypers.z_binsize
    mybins = np.arange(np.amin(img_as_array), np.amax(img_as_array) + binwidth, binwidth)

    number, value = np.histogram(img_as_array, bins=mybins)
    topmost = np.argmax(number)
    return value[topmost]


def shift_img(img, binwidth=None):
    shifted = img - get_img_centre(img, binwidth)
    return shifted


def get_contour_heights(img, raw_xcoord, raw_ycoord):
    xcoord = [int(i) for i in raw_xcoord]
    ycoord = [int(i) for i in raw_ycoord]
    height = list(map(lambda x, y: img[y][x], xcoord, ycoord))
    return np.mean(height)


def points_in_bounds(img, x, y):
    limits = img.shape
    in_grid = True

    for i in range(len(x)):
        if x[i] < 0 or x[i] > limits[0] - 1:
            in_grid = False
        if y[i] < 0 or y[i] > limits[1] - 1:
            in_grid = False

    return in_grid


def get_gradients(img, raw_xcoord, raw_ycoord):
    xgrads, ygrads = np.gradient(img)
    xcoord = [int(i) for i in raw_xcoord]
    ycoord = [int(i) for i in raw_ycoord]

    x_contour_grads, y_contour_grads, p_contour_grads = [], [], []
    for i in range(len(xcoord)):
        xg = xgrads[ycoord[i]][xcoord[i]]
        yg = ygrads[ycoord[i]][xcoord[i]]
        pg = (xg**2 + yg**2)**0.5
        x_contour_grads.append(np.abs(xg))
        y_contour_grads.append(np.abs(yg))
        p_contour_grads.append(pg)

    xgrad, ygrad, pgrad = np.mean(p_contour_grads), np.mean(p_contour_grads), np.mean(p_contour_grads)
    return xgrad, ygrad, pgrad


def get_contour_frame(img, contours=None, threshold=None):
    if contours is None:
        contours = get_contours(img, threshold=threshold)
    contourlengths = [len(c) for c in contours]
    transposed = [c.transpose() for c in contours]
    xvar = [np.var(c[1]) for c in transposed]
    yvar = [np.var(c[0]) for c in transposed]
    center = [[int(np.mean(c[1])), int(np.mean(c[0]))] for c in transposed]
    height_at_center = [img[coord[1]][coord[0]] for coord in center]
    contourheights = [get_contour_heights(img, c[1], c[0]) for c in transposed]

    circle_err2, circle_rad = [], []
    for c in contours:
        r, err = fit_to_circle(c)
        circle_rad.append(r)
        circle_err2.append(err)

    xgrads, ygrads, perpgrads = [], [], []
    for c in transposed:
        xg, yg, pg = get_gradients(img, c[1], c[0])
        xgrads.append(xg)
        ygrads.append(yg)
        perpgrads.append(pg)

    # This could be coded more compactly, but it's left like this for ease in troubleshooting
    cats = [
        contourlengths,
        height_at_center,
        xvar,
        yvar,
        circle_err2,
        circle_rad,
        contourheights,
        perpgrads,
        xgrads,
        ygrads
    ]

    keys = [
        'contour_length',
        'centerheight',
        'x_variance',
        'y_variance',
        'circle_err2',
        'circle_rad',
        'contour_height',
        'perp_height_gradient',
        'x_height_gradient',
        'y_height_gradient'
    ]

    frame = DataFrame(
        np.asmatrix(cats).T,
        columns=keys
    )
    return frame


def fit_to_circle(contour):
    x, y = contour[:, 0], contour[:, 1]
    x_avg, y_avg = np.mean(x), np.mean(y)

    u, v = x - x_avg, y - y_avg  # reduced coordinates

    # sums emerging from least-squares solution
    Suv = np.sum(u * v)
    Suu = np.sum(u**2)
    Svv = np.sum(v**2)
    Suuv = np.sum(u**2 * v)
    Suvv = np.sum(u * v**2)
    Suuu = np.sum(u**3)
    Svvv = np.sum(v**3)

    # solve the system of equations
    A = np.array([[Suu, Suv], [Suv, Svv]])
    B = np.array([Suuu + Suvv, Svvv + Suuv]) / 2.0

    try:
        u_center, v_center = np.linalg.solve(A, B)
    except np.linalg.linalg.LinAlgError:
        u_center, v_center = np.mean(u), np.mean(v)

    x_center, y_center = u_center + x_avg, v_center + y_avg

    all_r = np.sqrt((x - x_center)**2 + (y - y_center)**2)
    r = np.mean(all_r)
    err2 = np.mean((r - all_r)**2)
    return r, err2


def get_circle(contour, r=None):
    x, y = contour[:, 0], contour[:, 1]
    x_avg, y_avg = np.mean(x), np.mean(y)

    if r is None:
        r, err = fit_to_circle(contour)

    theta = np.arange(0, 2 * np.pi, 1 / 360.0)
    x_soln = x_avg + r * np.cos(theta)
    y_soln = y_avg + r * np.sin(theta)

    return x_soln, y_soln


def get_coverage_fraction(img, threshold=None):
    if threshold is None:
        threshold = hypers.z_threshold

    img_as_array = np.asarray(np.asmatrix(img).flatten())[0]
    highpoints = [1 if i > threshold else 0 for i in img_as_array]

    return np.sum(highpoints) / float(len(highpoints))


def describe_img(img, contours=None, threshold=None, binwidth=None):
    if threshold is None:
        threshold = hypers.z_threshold
    if binwidth is None:
        binwidth = hypers.z_binsize

    frame = get_contour_frame(img, contours=contours, threshold=threshold)
    summary = {
        'z_binsize': binwidth,
        'z_threshold': threshold,
        'mean_xvar': np.mean(frame['x_variance']),
        'mean_yvar': np.mean(frame['y_variance']),
        'total_contours': len(frame.index),
        'mean_centerheight': np.mean(frame['centerheight']),
        'coverage_fraction': get_coverage_fraction(img, threshold),
        'mean_contoursize': np.mean(frame['contour_length']),
        'img_meanheight': np.mean(img),
        'img_variance': np.var(img),
    }
    return frame, summary


def describe_raw_img(img, threshold=None, binwidth=None):
    shifted_img = shift_img(img, binwidth)
    frame, summary = describe_img(shifted_img, threshold, binwidth)
    return frame, summary


def plot_contours(
        matrix, contours=None, threshold=None, interactive=False,
        points=None, long_ax=1000, x_units='nm', z_units='nm'
):
    if interactive:
        plt.ion()
    else:
        plt.ioff()

    if threshold is None:
        threshold = hypers.z_threshold

    if contours is None:
        contours = get_contours(matrix, threshold)

    fig, ax = plt.subplots(figsize=(8, 8))
    im = ax.imshow(matrix, interpolation='nearest', cmap=plt.cm.gray)

    for contour in contours:
        ax.plot(contour[:, 1], contour[:, 0], linewidth=1.0, color=sns.xkcd_rgb["pumpkin"])

    if points is not None:
        for xy_coord in points:
            plt.plot(xy_coord[0], xy_coord[1], 'o', color=sns.xkcd_rgb["teal"])

    ax.axis('image')
    ax.set_xticks([])
    ax.set_yticks([])
    add_colorbar(im, matrix, units=z_units)
    add_scalebar(ax, matrix, long_ax, units=x_units)

    plt.show()


def get_contours(matrix, threshold=None):
    if threshold is None:
        threshold = hypers.z_threshold

    matrix = np.asarray(matrix)
    contours = measure.find_contours(matrix, threshold)
    return contours


def plot_surface(matrix, long_ax=1000, x_units='nm', z_units='nm'):
    plt.figure(figsize=(8, 8))
    ax = plt.gca()
    im = plt.imshow(matrix, cmap=plt.cm.gray)
    ax.set_xticks([])
    ax.set_yticks([])
    add_colorbar(im, matrix, units=z_units)
    add_scalebar(ax, matrix, long_ax, units=x_units)

    plt.show()


def add_scalebar(ax, matrix, long_ax, units=None):
    pixelsize = float(long_ax) / max(matrix.shape)
    barsize = int(pixelsize * 0.1 * matrix.shape[1])

    scalebar = anchored_artists.AnchoredSizeBar(
        ax.transData,
        0.1 * matrix.shape[1],
        '{} {}'.format(barsize, units),
        loc=1,
        pad=0.05,
        color='white',
        size_vertical=4,
        frameon=False
    )

    ax.add_artist(scalebar)


def add_colorbar(im, matrix, aspect=20, units=None):
    divider = axes_grid1.make_axes_locatable(im.axes)
    width = axes_grid1.axes_size.AxesY(im.axes, aspect=1.0 / aspect)
    current_ax = plt.gca()
    cax = divider.append_axes('right', size=width, pad=0.05)
    plt.sca(current_ax)

    cbar = im.axes.figure.colorbar(im, cax=cax, ticks=[np.amin(matrix), 0, np.amax(matrix)])
    if isinstance(units, str):
        cbar.ax.set_yticklabels([
            str(np.amin(matrix)) + " " + units,
            '0' + units,
            str(np.amax(matrix)) + " " + units
        ])
    return cbar


def load_scan(fname, asarray=True, ftype=None):
    if ftype is None:
        ext = fname.split('.')[-1]
    else:
        ext = ftype

    if ext in ['txt', 'ascii']:
        height, phase = _load_ascii(fname)
    elif ext in ['tif', 'png', 'eps']:
        height, phase = _load_img(fname)

    if asarray:
        height, phase = np.asarray(height), np.asarray(phase)

    return height, phase


def _load_img(fname):
    img = Image.open(fname)
    r, g, b = img.split()
    greyscale = 0.2989 * r + 0.5870 * g + 0.1140 * b

    return greyscale, None


def _load_ascii(fname):
    fin = open(fname)
    fin = fin.readlines()

    data = []
    for i in fin:
        if "\\" not in i:
            if "?" not in i:
                line = i.split()
                fixedline = []
                for k in line:
                    fixedline.append(float(k))
                data.append(fixedline)

    textblock = 0
    height = []
    phase = []

    for i in range(len(data)):
        if len(data[i]) < 1:
            textblock += 1
        elif len(data[i]) > 0:
            if textblock == 1:
                height.append(data[i])
            elif textblock == 2:
                phase.append(data[i])

    return height, phase


def clear_plots():
    plt.close('all')
