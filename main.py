'''
Interactively use software to manually or automatically classify AFM data

Usage:
    main.py <path_to_images> <path_to_db>

Options:
    -h --help   Print this usage

'''
from classify.ui import choose, ask
from docopt import docopt
from db_man import DBMan
import pickle
import hypers
from classify import label_data
import os
import img_functions


class Main(object):

    def __init__(self, db_path, img_dir, img_extension='.txt', verbose=True):
        self.DBM = DBMan(db_path, load_labelling=True)
        self.img_dir = img_dir
        self.model = None
        self.modelname = None
        self.modelfolder = 'classify/saved_models'
        self.img_extension = img_extension
        self.verbose = verbose
        self.prhyp = hypers.to_dict()  # processing hypers
        self.mainloop()

    def mainloop(self):
        options = [
            'Set processing parameters',
            'Single image to database',
            'Image directory to database',
            'Manually classify image contours',
            'Train new Model',
            'Predict using current Model',
            'Save using current Model',
            'Load Trained Model',
            'Save model predictions',
            'Analyze model predictions'
        ]
        self.print_display()
        task = choose('Which task do you want to perform?', options)

        if task == 'Set processing parameters':
            self.set_processing_parameters()
        elif task == 'Single image to database':
            self.save_one_image()
        elif task == 'Image directory to database':
            self.save_one_dir()
        elif task == 'Manually classify image contours':
            self.initialize_manual_classification()
        elif task == 'Train new Model':
            self.train_model()
        elif task == 'Predict using current Model':
            self.make_model_predict()
        elif task == 'Save using current Model':
            self.save_model()
        elif task == 'Load Trained Model':
            self.load_model()
        elif task == 'Save model predictions':
            self.save_predictions()
        elif task == 'Analyze model predictions':
            self.analyze_predictions()

    # TODO use curses to make this display at top
    def print_display(self):
        print('\n\n')
        print('################################################')
        print("### PROTUSION RECOGNITION AND CLASSIFICATION ###")
        print('################################################')
        print("\nCURRENT MODEL:\t {}".format(self.modelname))
        hyperstrings = ['\n\t{} = {}'.format(key, self.prhyp[key]) for key in self.prhyp]
        print("IMAGE PROCESSING HYPERPARAMS: {}".format(''.join(hyperstrings)))

    # TODO add dir changing functionality
    def save_one_image(self):
        img_name = ask('What is the image called?')
        path_to_img = os.path.join(self.img_dir, img_name)
        while not os.path.isfile(path_to_img):
            img_name = ask('No file found at {}. Try another name?'.format(path_to_img))
            path_to_img = os.path.join(self.img_dir, img_name)

        self._save_and_process(path_to_img)

    # TODO add dir changing functionality
    def save_one_dir(self):
        files_to_process = []
        for filename in os.listdir(self.img_dir):
            if self.img_extension in filename:
                files_to_process.append(os.path.join(self.img_dir, filename))

        for idx, sympath in enumerate(files_to_process):
            self._save_and_process(sympath)
            if self.verbose:
                print('Processed file {} of {}.'.format(idx + 1, len(files_to_process)))

    def initialize_manual_classification(self):
        ManClass = label_data.LabelData(self.DBM, self.img_dir)
        ManClass.choose_z_params()
        ManClass.label()

    def train_model(self):
        print('Unfinished')

    def make_model_predict(self):
        print('Unfinished')

    def save_model(self):
        modelname = ask('What would you like to call this model?', str)
        pickle.dump(self.model, modelname)

    def load_model(self):
        print('Unfinished')

    def save_predictions(self):
        print('Unfinished')

    def analyze_predictions(self):
        print('Open jupyter notebook or R')

    def set_processing_parameters(self):
        for key in self.prhyp:
            self.prhyp[key] = ask(
                'Choose value for {}\t (currently set to {})'.format(
                    key, self.prhyp[key]
                ), float
            )

    def _save_and_process(self, path_to_img):
        imginfo = self.DBM.ImgMan.info_from_link(path_to_img)
        img = imginfo.get_height_img()
        img = img_functions.shift_img(img, binwidth=self.prhyp['z_binsize'])

        contourdata, contoursummary = img_functions.describe_img(
            img, binwidth=self.prhyp['z_binsize'], threshold=self.prhyp['z_threshold']
        )

        self.DBM.ImgMan.save_img_info(imginfo)

        procsum = self.DBM.ProcMan.save_processing_summary(
            contoursummary,
            ImgInfo=imginfo,
            z_binsize=self.prhyp['z_binsize'],
            z_threshold=self.prhyp['z_threshold']
        )
        self.DBM.ContMan.save_contours(
            contourdata,
            ProcessingSummary=procsum
        )


if __name__ == "__main__":
    args = docopt(__doc__)
    db_path = args['<path_to_db>']
    img_dir = args['<path_to_images>']
    main = Main(db_path, img_dir)
    main.mainloop()
