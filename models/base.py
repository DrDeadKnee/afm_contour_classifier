import sqlite3
import pandas


class MissingFieldError(Exception):
    pass


class MissingRecordError(Exception):
    pass


class NotImplementedError:
    pass


class BaseModel(object):

    @classmethod
    def from_sql(cls, db_row):
        new_model = cls()
        return cls._populate_model_from_mapped_values(
            new_model, db_row, "DB query return"
        )

    @classmethod
    def _populate_model_from_mapped_values(cls, model, mapped, error_prefix):
        for attribute in dir(model):
            if attribute.startswith("__"):
                continue
            if callable(getattr(model, attribute)):
                continue
            try:
                setattr(model, attribute, mapped[attribute])
            except(KeyError, IndexError):
                raise MissingFieldError(
                    "{} has no value '{}'.\n The model class {} expects this value".format(
                        error_prefix, attribute, cls.__name__
                    )
                )
        return model

    def populate_from_dict(self, modeldict):
        for key in modeldict:
            setattr(self, key, modeldict[key])

    def to_sql_insert(self):
        header = "INSERT INTO {} ( ".format(self.__tablename__)
        fields = ""
        values = ") VALUES ("
        modeldict = self.to_dict()

        for key in modeldict:
            fields += " {},".format(key)
            values += " :{},".format(key)
        sql = header + fields[:-1] + values[:-1] + ")"

        return sql, modeldict

    def to_sql_update(self):
        modeldict = self.to_dict()
        header = "UPDATE {} SET ".format(self.__tablename__)

        fields = ""
        for key in modeldict:
            fields += "{}=:{}, ".format(key, key)

        footer = " WHERE rowid=:rowid"
        sql = header + fields[:-2] + footer
        return sql, modeldict

    def to_dict(self):
        raise NotImplementedError("Must be implemented in subclass")

    def __repr__(self):
        return repr(self.to_dict())


class BaseManager(object):

    def __init__(self, cnx):
        self.cnx = cnx
        self.cnx.row_factory = sqlite3.Row

    def create(self, model):
        if self.__one2one__:
            self._create_single(model)
        else:
            self._create_many(model)

    def _create_many(self, model):
        df = model.to_dataframe()
        df.to_sql(
            self.__modelclass__.__tablename__,
            con=self.cnx,
            if_exists='append',
            index=False
        )
        return model

    def _create_single(self, model):
        cur = self.cnx.cursor()
        sql, args = model.to_sql_insert()
        cur.execute(sql, args)
        new_id = cur.lastrowid
        self.cnx.commit()
        model.rowid = new_id
        return model

    def read_unique(self, column_name, limit=1000, skip=0):
        cur = self.cnx.cursor()
        sql = "SELECT DISTINCT {} FROM {} LIMIT ? OFFSET ?".format(
            column_name, self.__modelclass__.__tablename__
        )
        cur.execute(sql, (limit, skip))
        result = cur.fetchone()
        while result:
            yield result
            result = cur.fetchone()

    def read(self, rowid):
        cur = self.cnx.cursor()
        sql = "SElECT * FROM {} WHERE rowid = ?".format(
            self.__modelclass__.__tablename__
        )
        cur.execute(sql, (rowid,))
        model = self.__modelclass__.from_sql(cur.fetchone())
        return model

    def update(self, model):
        if self.__one2one__:
            model = self._update_single(model)
        elif not self.__one2one__:
            model = self._update_many(model)
        return model

    def _update_single(self, model):
        cur = self.cnx.cursor()
        sql, args = model.to_sql_update()
        cur.execute(sql, args)
        self.cnx.commit()
        return model

    def _update_many(self, model):
        df = model.to_dataframe()
        df.to_sql(
            self.__modelclass__.__tablename__,
            con=self.cnx,
            if_exists='append',
            index=False
        )
        return model

    def delete(self, model):
        cur = self.cnx.cursor()
        sql = "DELETE FROM {} WHERE rowid = ?".format(
            self.__modelclass__.__tablename__
        )
        cur.execute(sql, (model.rowid,))
        self.cnx.commit()

    def list(self, limit=1000, skip=0):
        cur = self.cnx.cursor()
        sql = "SELECT rowid, * FROM {} LIMIT ? OFFSET ?".format(
            self.__modelclass__.__tablename__
        )
        cur.execute(sql, (limit, skip))
        result_row = cur.fetchone()
        while result_row:
            result_model = self.__modelclass__.from_sql(result_row)
            yield result_model
            result_row = cur.fetchone()

    def _list_as_df(self, limit=1000, skip=0):
        sql = "SELECT * FROM {} LIMIT {} OFFSET {}".format(
            self.__modelclass__.__tablename__, limit, skip
        )
        df = pandas.read_sql(sql, con=self.cnx, index_col='rowid')
        return df

    def find(self, filter_args):
        if not filter_args:
            raise Exception("Don't use find without args, use list instead")

        where_clause_args = []
        for key in filter_args:
            argstring = "{}=:{}".format(key, key)
            where_clause_args.append(argstring)

        args_for_query = where_clause_args[0]
        if len(where_clause_args) > 1:
            for i in where_clause_args[1:]:
                args_for_query += " AND {}".format(i)

        sql = "SELECT * FROM {} WHERE {}".format(
            self.__modelclass__.__tablename__, args_for_query
        )

        if self.__one2one__:
            hits = [i for i in self._find_as_single(sql, filter_args)]
            if len(hits) == 1:
                model = hits[0]
            else:
                model = hits

        elif not self.__one2one__:
            model = self._find_as_dataframe(filter_args)

        return model

    def _find_as_dataframe(self, filter_args):
        header = "SELECT * FROM {} WHERE".format(self.__modelclass__.__tablename__)
        body = [" {}={} AND".format(key, filter_args[key]) for key in filter_args]
        sql = header + body[:-4]
        df = pandas.read_sql(sql, con=self.cnx)

        return df

    def _find_as_single(self, sql, filter_args):
        cur = self.cnx.cursor()
        cur.execute(sql, filter_args)
        result_row = cur.fetchone()
        while result_row:
            result_model = self.__modelclass__.from_sql(result_row)
            yield result_model
            result_row = cur.fetchone()

    def find_most_recent(self, filter_args):
        all_instances = [i for i in self.find(filter_args)]
        if len(all_instances) > 0:
            response = all_instances[-1]
        else:
            response = None

        return response

    def load_most_recent(self):
        all_instances = [i for i in self.list()]
        if len(all_instances) > 0:
            response = all_instances[-1]
        else:
            response = None

        return response

    def get_fields(self):
        cur = self.cnx.cursor()
        sql = "SELECT * FROM {}".format(self.__modelclass__.__tablename__)
        cur.exectute(sql)
        fields = [desc[0] for desc in cur.desription]
        return fields
