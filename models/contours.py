from models.base import BaseModel, BaseManager
import pandas
from processingsummary import ProcessingSummaryManager


class BadArgumentError(Exception):
    pass


class Contours(BaseModel):
    __tablename__ = "Contours"

    def __init__(
        self,
        rowid=None,
        proc_id=None,
        contour_length=None,
        centerheight=None,
        x_variance=None,
        y_variance=None,
        circle_err2=None,
        circle_rad=None,
        contour_height=None,
        perp_height_gradient=None,
        x_height_gradient=None,
        y_height_gradient=None
    ):
        self.rowid = rowid
        self.proc_id = proc_id
        self.contour_length = contour_length
        self.centerheight = centerheight
        self.x_variance = x_variance
        self.y_variance = y_variance
        self.circle_err2 = circle_err2
        self.circle_rad = circle_rad
        self.contour_height = contour_height
        self.perp_height_gradient = perp_height_gradient
        self.x_height_gradient = x_height_gradient
        self.y_height_gradient = y_height_gradient

    def to_dict(self):
        return {
            'rowid': self.rowid,
            'proc_id': self.proc_id,
            'contour_length': self.contour_length,
            'centerheight': self.centerheight,
            'x_variance': self.x_variance,
            'y_variance': self.y_variance,
            'circle_err2': self.circle_err2,
            'circle_rad': self.circle_rad,
            'contour_height': self.contour_height,
            'perp_height_gradient': self.perp_height_gradient,
            "x_height_gradient": self.x_height_gradient,
            "y_height_gradient": self.y_height_gradient
        }

    def to_dataframe(self):
        return pandas.DataFrame.from_dict(self.to_dict())

    def update_from_dataframe(self, self_as_df):
        self.rowid = self_as_df['rowid'].tolist()
        self.proc_id = self_as_df['proc_id'].tolist()
        self.contour_length = self_as_df['contour_length'].tolist()
        self.centerheight = self_as_df['centerheight'].tolist()
        self.x_variance = self_as_df['x_variance'].tolist()
        self.y_variance = self_as_df['y_variance'].tolist()


class ContourManager(BaseManager):
    __modelclass__ = Contours
    __one2one__ = False

    def save_contours(self, contour_data, ProcessingSummary=None):
        if isinstance(contour_data, pandas.DataFrame):
            if ProcessingSummary is None:
                raise BadArgumentError(
                    "require ProcessingSummary object as argument to create contours from dataframe"
                )
            else:
                contours = self.contours_from_df(contour_data, ProcessingSummary)
        elif isinstance(contour_data, Contours):
            contours = contour_data
        self.create(contours)

        if contours.rowid is None:
            return self.load_most_recent()
        else:
            return contours

    def contours_from_df(self, contour_data, ProcessingSummary):
        assert isinstance(contour_data, pandas.DataFrame)
        new_contour_obj = self.setup_new_contours(ProcessingSummary)
        new_contour_obj.contour_length = contour_data['contour_length']
        new_contour_obj.centerheight = contour_data['centerheight']
        new_contour_obj.x_variance = contour_data['x_variance']
        new_contour_obj.y_variance = contour_data['y_variance']
        new_contour_obj.circle_err2 = contour_data['circle_err2']
        new_contour_obj.circle_rad = contour_data['circle_rad']
        new_contour_obj.contour_height = contour_data['contour_height']
        new_contour_obj.perp_height_gradient = contour_data['perp_height_gradient']
        new_contour_obj.x_height_gradient = contour_data["x_height_gradient"]
        new_contour_obj.y_height_gradient = contour_data["y_height_gradient"]

        return new_contour_obj

    def setup_new_contours(self, ProcessingSummary):
        PSM = ProcessingSummaryManager(self.cnx)
        assert isinstance(ProcessingSummary, PSM.__modelclass__)

        if ProcessingSummary.rowid is None:
            ProcessingSummary = PSM.save_processing_summary(ProcessingSummary)

        return Contours(
            proc_id=ProcessingSummary.rowid
        )
