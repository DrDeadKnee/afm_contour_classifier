from models.base import BaseModel, BaseManager
import img_functions
from os import path


class BadArgumentError(Exception):
    pass


class ImgInfo(BaseModel):
    __tablename__ = 'ImgInfo'

    def __init__(
        self,
        rowid=None,
        concentration=None,
        time=None,
        img_name=None,
        img_path=None,
        sample=None,
        scan=None,
        quality=None,
        notes=None
    ):
        self.rowid = rowid
        self.concentration = concentration
        self.time = time
        self.img_name = img_name
        self.img_path = img_path
        self.sample = sample
        self.scan = scan
        self.quality = quality
        self.notes = notes

    def to_dict(self):
        return {
            "rowid": self.rowid,
            "concentration": self.concentration,
            "time": self.time,
            "img_name": self.img_name,
            "img_path": self.img_path,
            "sample": self.sample,
            "scan": self.scan,
            "quality": self.quality,
            "notes": self.notes
        }

    def get_height_img(self):
        try:
            height, phase = img_functions.load_scan(self.img_path)
        except UnboundLocalError:
            print("Path {} is empty".format(self.img_path))

        return height


class ImgInfoManager(BaseManager):
    __modelclass__ = ImgInfo
    __one2one__ = True

    def save_img_info(self, imgthing, **kwargs):

        if isinstance(imgthing, ImgInfo):
            self.populate_from_variable_args(imgthing, **kwargs)
            img_to_write = imgthing

        elif isinstance(imgthing, str):
            try:
                img_to_write = self.info_from_link(imgthing, **kwargs)
            except AssertionError:
                raise BadArgumentError(
                    "save_img_info requires either a valid path to an image or"
                    " an ImgInfo object as primary argument."
                )

        if img_to_write.rowid is None:
            self.create(img_to_write)
            return self.load_most_recent()
        elif isinstance(img_to_write.rowid, int):
            img_to_write = self.update(img_to_write)
            return img_to_write

    def populate_from_variable_args(self, img_info_obj, **kwargs):
        asdict = img_info_obj.to_dict()
        for key in kwargs:
            if key in asdict.keys():
                setattr(img_info_obj, key, kwargs[key])
            else:
                print('ImgInfo model has no slot for {} argument'.format(key))
        return img_info_obj

    # Assumes the naming syntax 'A1s.txt' --> sample A, scan 1, quality s
    def info_from_link(self, link_to_img, **kwargs):
        cur = self.cnx.cursor()
        sql = "SELECT COUNT(rowid) FROM ImgInfo WHERE img_path=?"
        cur.execute(sql, (link_to_img,))
        count = [i for i in cur.fetchone()][0]

        if count == 0:
            new_img = self._new_info_from_link(link_to_img, **kwargs)
        elif count == 1:
            old_img = self.find({'img_path': link_to_img})
            new_img = self._merge_info(old_img, **kwargs)
        elif count > 1:
            print("WARNING: More than one copy of Image in database. Loading most recent info...")
            old_img = self.find({'img_path': link_to_img})[-1]
            new_img = self._merge_info(old_img, **kwargs)

        return new_img

    def _merge_info(self, old_img, **kwargs):
        new_img_dict = old_img.to_dict()
        for key in kwargs:
            if key in new_img_dict.keys() and key != 'rowid':
                new_img_dict[key] = kwargs[key]

        new_img = ImgInfo()
        new_img = self.populate_from_variable_args(new_img, **new_img_dict)
        return new_img

    def _new_info_from_link(self, link_to_img, **kwargs):
        assert(path.isfile(link_to_img))
        new_img = ImgInfo(img_path=link_to_img)
        head, filename = path.split(link_to_img)
        new_img.img_name = filename.split('.')[0]

        if 'sample' in kwargs:
            new_img.sample = kwargs['sample']
        else:
            new_img.sample = new_img.img_name[0]

        if 'scan' in kwargs:
            new_img.scan = kwargs['scan']
        else:
            new_img.scan = new_img.img_name[1]

        if 'quality' in kwargs:
            new_img.quality = kwargs['quality']
        elif 'quality' not in kwargs and len(new_img.img_name) >= 3:
            new_img.quality = new_img.img_name[2]

        if 'notes' in kwargs:
            new_img.notes = kwargs['notes']

        return new_img
