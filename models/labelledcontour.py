from models.base import BaseModel, BaseManager
import pandas
import img_functions


class BadArgumentError(Exception):
    pass


class LabelledContour(BaseModel):
    __tablename__ = "LabelledContour"

    def __init__(
        self,
        rowid=None,
        img_name=None,
        z_binsize=None,
        z_threshold=None,
        contour_length=None,
        centerheight=None,
        x_variance=None,
        y_variance=None,
        circle_err2=None,
        circle_rad=None,
        label=None,
        is_contour=None,
        contour_height=None,
        perp_height_gradient=None,
        x_height_gradient=None,
        y_height_gradient=None,
        img_meanheight=None,
        img_variance=None
    ):
        self.rowid = rowid
        self.contour_length = contour_length
        self.centerheight = centerheight
        self.x_variance = x_variance
        self.y_variance = y_variance
        self.circle_err2 = circle_err2
        self.circle_rad = circle_rad
        self.img_name = img_name
        self.z_binsize = z_binsize
        self.z_threshold = z_threshold
        self.label = label
        self.is_contour = is_contour
        self.contour_height = contour_height
        self.perp_height_gradient = perp_height_gradient
        self.x_height_gradient = x_height_gradient
        self.y_height_gradient = y_height_gradient
        self.img_meanheight = img_meanheight
        self.img_variance = img_variance

    def to_dict(self):
        return {
            "rowid": self.rowid,
            "contour_length": self.contour_length,
            "centerheight": self.centerheight,
            "x_variance": self.x_variance,
            "y_variance": self.y_variance,
            "circle_err2": self.circle_err2,
            "circle_rad": self.circle_rad,
            "img_name": self.img_name,
            "z_binsize": self.z_binsize,
            "z_threshold": self.z_threshold,
            "label": self.label,
            "is_contour": self.is_contour,
            "contour_height": self.contour_height,
            "perp_height_gradient": self.perp_height_gradient,
            "x_height_gradient": self.x_height_gradient,
            "y_height_gradient": self.y_height_gradient,
            "img_meanheight": self.img_meanheight,
            "img_variance": self.img_variance
        }


class LabelledContourManager(BaseManager):
    __modelclass__ = LabelledContour
    __one2one__ = True  # Default 1 to 1. Whole dataframe load/save is accesible.

    def save_data(self, data):
        if isinstance(data, LabelledContour):
            self.create(data)
        elif isinstance(data, pandas.DataFrame):
            rows = data.to_dict('records')
            for row in rows:
                newmodel = LabelledContour()
                newmodel.populate_from_dict(row)
                self.create(newmodel)

    def populate_from_contour(self, contour, img, z_threshold=None, z_binsize=None):
        if len(contour) == 1 and len(contour[0]) > 1:
            pass
        else:
            raise BadArgumentError(
                "This function expects a single contour contained in a list."
            )
        frame, summary = img_functions.describe_img(
            img, contour, threshold=z_threshold, binwidth=z_binsize
        )

        new_entry = LabelledContour(
            z_binsize=z_binsize,
            z_threshold=z_threshold,
            contour_length=summary['mean_contoursize'],
            centerheight=summary['mean_centerheight'],
            x_variance=summary['mean_xvar'],
            y_variance=summary['mean_yvar'],
            circle_err2=frame['circle_err2'][0],
            circle_rad=frame['circle_rad'][0],
            contour_height=frame['contour_height'][0],
            perp_height_gradient=frame['perp_height_gradient'][0],
            y_height_gradient=frame['x_height_gradient'][0],
            x_height_gradient=frame['y_height_gradient'][0],
            img_meanheight=summary['img_meanheight'],
            img_variance=summary['img_variance']
        )
        return new_entry

    def load_new_df(self):
        dummy_entry = LabelledContour()
        keys = [key for key in dummy_entry.to_dict()]
        df = pandas.DataFrame(columns=keys)
        return df

    def load_all_labels(self):
        return self._list_as_df()

    def split_labels(self):
        df = self._list_as_df()
        labels = ['label', 'is_contour']
        Y = [df[i] for i in labels]
        X = df.drop(labels + ['img_name'], axis=1)

        return Y, X
