from models.base import BaseModel, BaseManager
import hypers
from imginfo import ImgInfoManager


class BadArgumentError(Exception):
    pass


class ProcessingSummary(BaseModel):
    __tablename__ = "ProcessingSummary"

    def __init__(
        self,
        rowid=None,
        img_id=None,
        z_binsize=None,
        z_threshold=None,
        total_contours=None,
        mean_xvar=None,
        mean_yvar=None,
        mean_centerheight=None,
        coverage_fraction=None,
        mean_contoursize=None,
        img_meanheight=None,
        img_variance=None
    ):
        self.rowid = rowid
        self.img_id = img_id
        self.z_binsize = z_binsize
        self.z_threshold = z_threshold
        self.mean_xvar = mean_xvar
        self.mean_yvar = mean_yvar
        self.mean_centerheight = mean_centerheight
        self.coverage_fraction = coverage_fraction
        self.mean_contoursize = mean_contoursize
        self.total_contours = total_contours
        self.img_meanheight = img_meanheight
        self.img_variance = img_variance

    def to_dict(self):
        return {
            'rowid': self.rowid,
            'img_id': self.img_id,
            'z_binsize': self.z_binsize,
            'z_threshold': self.z_threshold,
            'total_contours': self.total_contours,
            'mean_xvar': self.mean_xvar,
            'mean_yvar': self.mean_yvar,
            'mean_centerheight': self.mean_centerheight,
            'coverage_fraction': self.coverage_fraction,
            'mean_contoursize': self.mean_contoursize,
            'img_meanheight': self.img_meanheight,
            'img_variance': self.img_variance
        }


class ProcessingSummaryManager(BaseManager):
    __modelclass__ = ProcessingSummary
    __one2one__ = True

    def setup_new_process(self, ImgInfo, z_binsize=None, z_threshold=None):
        IIM = ImgInfoManager(self.cnx)
        # assert isinstance(ImgInfo, IIM.__modelclass__)

        if ImgInfo.rowid is None:
            ImgInfo = IIM.save_img_info(ImgInfo)

        if z_binsize is None:
            z_binsize = hypers.z_binsize
        if z_threshold is None:
            z_threshold = hypers.z_threshold

        return ProcessingSummary(
            img_id=ImgInfo.rowid,
            z_binsize=z_binsize,
            z_threshold=z_threshold
        )

    def save_processing_summary(self, data_summary, ImgInfo=None, z_binsize=None, z_threshold=None):
        if isinstance(data_summary, dict):
            if ImgInfo is None:
                raise BadArgumentError(
                    "require ImgInfo object as argument to create contour from dataframe"
                )
            else:
                ProcSum = self.summary_from_dict(data_summary, ImgInfo, z_binsize, z_threshold)

        elif isinstance(data_summary, ProcessingSummary):
            ProcSum = data_summary

        self.create(ProcSum)

        if ProcSum.rowid is None:
            return self.load_most_recent()
        elif isinstance(ProcSum.rowid, int):
            return ProcSum

    def summary_from_dict(self, data_summary, ImgInfo, z_binsize, z_threshold):
        assert isinstance(data_summary, dict)
        ProcSum = self.setup_new_process(ImgInfo, z_binsize, z_threshold)
        ProcSum.mean_xvar = data_summary['mean_xvar']
        ProcSum.mean_yvar = data_summary['mean_yvar']
        ProcSum.total_contours = data_summary['total_contours']
        ProcSum.mean_centerheight = data_summary['mean_centerheight']
        ProcSum.coverage_fraction = data_summary['coverage_fraction']
        ProcSum.mean_contoursize = data_summary['mean_contoursize']
        ProcSum.img_meanheight = data_summary['img_meanheight']
        ProcSum.img_variance = data_summary['img_variance']

        return ProcSum
