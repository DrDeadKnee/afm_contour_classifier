import db_man
import img_functions
import os


class BadArgumentError(Exception):
    pass


def process_one_img(path_to_img, path_to_db, shift=True, z_threshold=None, z_binsize=None):
    # set up database entry
    DBM = db_man.DBMan(path_to_db)

    # pre-process and correct image
    imginfo = DBM.ImgMan.info_from_link(path_to_img)
    img = imginfo.get_height_img()
    if shift:
        img = img_functions.shift_img(img, binwidth=z_binsize)

    # calculate and summarize contours
    contourdata, contoursummary = img_functions.describe_img(img, binwidth=z_binsize, threshold=z_threshold)

    # save data and summary through appropriate classes
    procsum = DBM.ProcMan.save_processing_summary(
        contoursummary,
        ImgInfo=imginfo,
        z_binsize=z_binsize,
        z_threshold=z_threshold
    )
    contours = DBM.ContMan.save_contours(
        contourdata,
        ProcessingSummary=procsum
    )
    return contours, procsum


# This is a bit inefficient because we connect to database and drop the connection for each image.
# That could be improved.
def process_directory(path_to_dir, path_to_db, z_threshold=None, z_binsize=None, img_ext='.txt'):
    files_to_process = []
    for i in os.listdir(path_to_dir):
        if img_ext in i:
            files_to_process.append(os.path.join(path_to_dir, i))

    for idx, filename in enumerate(files_to_process):
        process_one_img(filename, path_to_db, z_threshold=z_threshold, z_binsize=z_binsize)
        print('Processed file {} of {}.'.format(idx + 1, len(files_to_process)))


def fetch_visual_process(path_to_db, proc_id):
    DBM = db_man.DBMan(path_to_db)
    summary = DBM.ProcMan.find({'rowid': proc_id})
    imginfo = DBM.ImgMan.find({'rowid': summary.img_id})
    img = imginfo.get_height_img()
    img = img_functions.shift_img(img, binwidth=summary.z_binsize)
    img_functions.plot_contours(img, threshold=summary.z_threshold)


def fetch_whole_table(path_to_db, tabletype='augmented'):
    DBM = db_man.DBMan(path_to_db)

    if tabletype not in ['augmented', 'ImgInfo', 'ProcessingSummary']:
        raise BadArgumentError(
            "Optional argument 'tabletype' must be either augmented, ImgInfo, or ProcessingSummary"
        )
    elif tabletype == 'augmented':
        table = DBM.load_augmented_table()
    else:
        table = DBM.load_single_table(tabletype)

    return table
