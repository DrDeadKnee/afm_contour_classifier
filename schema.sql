PRAGMA foreign_keys = ON;

BEGIN TRANSACTION;
CREATE TABLE "ImgInfo" (
	`rowid`		INTEGER PRIMARY KEY,
	`concentration`	INTEGER,
	`time`		INTEGER,
	`img_path`	INTEGER,
	`img_name` 	INTEGER,
	`sample`	INTEGER,
	`scan`		INTEGER,
	`quality`	INTEGER,
	`notes`		INTEGER
);

CREATE TABLE "ProcessingSummary" (
	`rowid` 		INTEGER PRIMARY KEY,
	`img_id`		INTEGER,
	`z_binsize`		INTEGER,
	`z_threshold`		INTEGER,
	`img_meanheight`	INTEGER,
	`img_variance`		INTEGER,
	`mean_xvar`		INTEGER,
	`mean_yvar`		INTEGER,
	`total_contours`	INTEGER,
	`mean_centerheight`	INTEGER,
	`coverage_fraction`	INTEGER,
	`mean_contoursize`	INTEGER,
	FOREIGN KEY(img_id) REFERENCES ImgInfo(rowid) ON DELETE CASCADE
);

CREATE TABLE "Contours" (	--plural, intended to be read as one-to-many
	`rowid`			INTEGER PRIMARY KEY,
	`proc_id`		INTEGER,	
	`contour_length`	INTEGER,	--circumference of contour
	`centerheight`		INTEGER,
	`x_variance`		INTEGER,
	`y_variance`		INTEGER,
	`circle_err2`		INTEGER, 	--residuals to fit to a circle
	`circle_rad`		INTEGER, 	--best radius
	`contour_height`	INTEGER,
	`x_height_gradient`	INTEGER,
	`y_height_gradient`	INTEGER,
	`perp_height_gradient`	INTEGER,
	FOREIGN KEY(proc_id) REFERENCES ProcessingSummary(rowid) ON DELETE CASCADE
);

CREATE TABLE "LabelledContour" (
	`rowid`			INTEGER PRIMARY KEY,
	`img_name`		INTEGER,
	`z_binsize`		INTEGER,
	`z_threshold`		INTEGER,
	`contour_length`	INTEGER,
	`centerheight`		INTEGER,
	`x_variance`		INTEGER,
	`y_variance`		INTEGER,
	`circle_err2`		INTEGER,
	`circle_rad`		INTEGER,
	`label`			INTEGER,
	`is_contour`		INTEGER,
	`contour_height`	INTEGER,
	`x_height_gradient`	INTEGER,
	`y_height_gradient`	INTEGER,
	`perp_height_gradient`	INTEGER,	
	`img_meanheight`	INTEGER,
	`img_variance`		INTEGER
);

CREATE INDEX ProcSumIDX ON ProcessingSummary(img_id);
CREATE INDEX ContourIDX ON Contours(proc_id);

COMMIT;
