import db_man
from os import remove
import procedures


class forprofile(object):

    def __init__(self):
        self.path_to_db = '/tmp/test_db.db'
        self.impath = 'img/a0.txt'
        self.DBM = db_man.DBMan(self.path_to_db)

    def test_procedures(self):
        path_to_dir = 'img'
        contours, procsum = procedures.process_one_img(self.impath, self.path_to_db)
        self._check_model_is_filled(contours, exceptions=[
            # 'circle_err2', 'circle_rad',
            'is_contour', 'classification'
        ])
        self._check_model_is_filled(procsum)

        procedures.process_directory(path_to_dir, self.path_to_db)
        procedures.fetch_visual_process(self.path_to_db, proc_id=1)

        procedures.fetch_whole_table(self.path_to_db)
        procedures.fetch_whole_table(self.path_to_db, tabletype='ImgInfo')
        print('Procedures checked.')

    def cleanup(self):
        remove(self.path_to_db)

    def _check_model_is_filled(self, model, exceptions=[]):
        for key in model.to_dict():
            if key not in exceptions:
                assert getattr(model, key) is not None


if __name__ == "__main__":
    fp = forprofile()
    fp.test_procedures()
