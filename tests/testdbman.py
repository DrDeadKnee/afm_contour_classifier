import unittest
import db_man
import os
import img_functions
import procedures
import tempfile


class TestDBMan(unittest.TestCase):

    def setUp(self):
        self.path_to_db = os.path.join(tempfile.gettempdir(), 'test.db')
        self.impath = os.path.join('img', 'a0.txt')
        self.DBM = db_man.DBMan(self.path_to_db)

    def test_imginfo(self):
        img_info = self.DBM.ImgMan.info_from_link(self.impath)
        height = img_info.get_height_img()

        self.DBM.ImgMan.save_img_info(img_info)
        self.DBM.ImgMan.save_img_info(self.impath)
        self.DBM.ImgMan.save_img_info(self.impath, notes='My knee hurts.')
        hurtknee = self.DBM.ImgMan.find({'notes': "My knee hurts."})
        self.assertEqual(hurtknee.rowid, 1)

    def test_processingsummary(self):
        imginfo = self.DBM.ImgMan.info_from_link(self.impath)
        procsum = self.DBM.ProcMan.setup_new_process(imginfo)
        assert procsum.img_id is not None

        self.DBM.ProcMan.save_processing_summary(procsum)
        newprocsum = self.DBM.ProcMan.find({'rowid': 1})
        assert newprocsum.img_id == self.DBM.ImgMan.load_most_recent().rowid

    def test_contours(self):
        imginfo = self.DBM.ImgMan.info_from_link(self.impath)
        procsum = self.DBM.ProcMan.setup_new_process(imginfo)
        contours = self.DBM.ContMan.setup_new_contours(procsum)
        assert contours.proc_id is not None

        contourdata = img_functions.get_contour_frame(imginfo.get_height_img())
        contours = self.DBM.ContMan.contours_from_df(contourdata, procsum)
        contours = self.DBM.ContMan.save_contours(contours)
        assert contours.rowid is not None

    def test_procedures(self):
        path_to_dir = 'img'
        contours, procsum = procedures.process_one_img(self.impath, self.path_to_db)
        self._check_model_is_filled(contours, exceptions=[
            # 'circle_err2', 'circle_rad',
            'is_contour', 'classification'
        ])
        self._check_model_is_filled(procsum)

        procedures.process_directory(path_to_dir, self.path_to_db)
        procedures.fetch_visual_process(self.path_to_db, proc_id=1)

        procedures.fetch_whole_table(self.path_to_db)
        procedures.fetch_whole_table(self.path_to_db, tabletype='ImgInfo')

    def _check_model_is_filled(self, model, exceptions=[]):
        for key in model.to_dict():
            if key not in exceptions:
                assert getattr(model, key) is not None

    def tearDown(self):
        self.DBM.cnx.close()
        os.remove(self.path_to_db)
